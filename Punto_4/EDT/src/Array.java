public class Array extends Data {

    protected Data[] content;
    protected int size;
    protected Data def;

    public Array() {

    }

    public Array(int size, Data def, Output o){
        this.content = new Data[size];
        this.setOutput(o);
        this.size=size;
        this.def=def;
        for(int i=0;i<this.content.length;i++){
            this.content[i]=this.def;
        }
    }

    @Override
    public Data get(int p) {
        if(this.isLegal(p)) {
            return this.content[p];
        }else {
            System.out.println("Error, get");
            return new Char('x', this.out);
        }
    }

    @Override
    public void set(int p, Data v) {
        if(this.isLegal(p)){
            this.content[p] = v;
        }
    }

    @Override
    public void rem(int p) {
        if(this.isLegal(p)) {
            this.set(p,this.def);
        }
    }

    @Override
    public void del(int p){
        if(this.isLegal(p)){
            for(int i=p;i<this.size-1;i++){
                this.set(i,this.get(i+1));
            }
            this.rem(this.size-1);
        }
    }

    @Override
    public void ins(int p, Data v){
        if(this.isLegal(p)){
            for(int i=this.size-1; i>p;i--){
                this.set(i,this.get(i-1));
            }
            this.set(p,v);
        }
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public void print() {
        this.out.print(this);
    }

    public int length(){
        return content.length;
    }

    public boolean isLegal(int p){
        return(p>=0&&p<this.size);
    }

}
