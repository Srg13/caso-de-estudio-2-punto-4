
public class Char extends Array {

    protected char content;

    public Char(char c,Output o){
        super();
        this.setOutput(o);
        this.set(c);
    }

    public void set(char c){
        this.content=c;
    }

    @Override
    public void print() {
        this.out.print(this);
    }

}
