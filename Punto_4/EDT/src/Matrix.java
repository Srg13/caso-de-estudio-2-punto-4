public class Matrix extends Array{

    public Matrix(int width, int height, Data def, Output o) {
        this.size=height;
        this.setOutput(o);
        this.content=new Data[height];
        this.def=new Array(width,def,this.out);
        for(int i=0;i<height;i++){
            this.content[i]=new Array(width,def,this.out);
        }
    }

    public void ins(int x, int y, Data v){
        this.get(y).ins(x,v);
    }

    public void del(int x,int y){
        this.get(y).del(x);
    }

    @Override
    public void print() {
        this.out.print(this);
    }

}
