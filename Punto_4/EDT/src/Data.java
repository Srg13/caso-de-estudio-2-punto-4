
public abstract class Data {

    public Output out;
    public abstract Data get(int p);
    public abstract void set(int p, Data v);
    public abstract void rem(int p);
    public abstract void del(int p);
    public abstract void ins(int p, Data v);
    public abstract int getSize();
    public void setOutput(Output o){
        this.out=o;
    }
    public abstract void print();

}
