
public abstract class Output {

    public abstract void print(Char c);
    public abstract void print(Array a);
    public abstract void print(Matrix m);

}
