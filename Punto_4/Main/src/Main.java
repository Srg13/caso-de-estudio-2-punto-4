public class Main {

    public static void main(String[] args){
        execArray();
        execMatrix();
    }

    static void execArray() {
        Output out = new SimpleOutput();
        Array a = new Array(10,new Char('x',out),out);
        a.ins(3,new Char('h',out));
        a.ins(4,new Char('o',out));
        a.ins(5,new Char('l',out));
        a.ins(8,new Char('a',out));
        a.print();
        a.del(6);
        a.del(6);
        a.print();
    }

    static void execMatrix() {
        Output out = new SimpleOutput();
        Matrix m = new Matrix(10,5,new Char(' ',out),out);
        m.setOutput(out);
        m.ins(2,3,new Char('h',out));
        m.ins(3,3,new Char('o',out));
        m.ins(4,3,new Char('l',out));
        m.ins(6,3,new Char('a',out));
        m.del(5,3);
        m.del(2);
        m.print();
    }

}
